/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problem3;

import java.util.ArrayList;

/**
 *
 * @author 2ndyrGroupA
 */
public class Problem3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       ArrayList<Integer> paraNum= new ArrayList<Integer>();
          paraNum.add(3);
          paraNum.add(8);
          paraNum.add(23);
          paraNum.add(91);
          paraNum.add(6);
          paraNum.add(1);
      System.out.println("First order of Array List\n" + paraNum);
          paraNum.set(0,1);
          paraNum.set(1,3);
          paraNum.set(2,8);
          paraNum.set(4,91);
          paraNum.set(5,6);
      System.out.println("Order of Array List, minimum value in the front\n"+paraNum);
    }
    
}
