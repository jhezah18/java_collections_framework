/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problem.pkg5;

/**
 *
 * @author 2ndyrGroupA
 */
public class Problem5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Problem # 5: Compare and contrast the classic for loop versus  foreach.\n"
                + "       "+ "What are the pros and cons of both sides?\n");
    
 System.out.println("Advantage of foreach loop:\n"+"The possibility of programming error is eliminated.\n" +
"It makes the code more readable.\n" +
"There is no use of index or rather a counter in this loop.\n");
 
 System.out.println("Disadvantage of for-each loop:\n"+"It cannot traverse through the elements in reverse fashion.\n" +
"You cannot skip any element as the concept of index is not there.\n" +
"You cannot choose to traverse to odd or even indexed elements too.\n");
 
 System.out.println(" Advantage of For Loop:\n"+"Execute blocks of code over and over again\n" +
        " We know exactly how many times the loop will execute before the loop starts\n");
 System.out.println("For loop Disadvantage:\n "+" It cannot traverse through the elements in reverse fashion"+
         "You cannot skip any element as the concept of index is not there\n"+
         "You cannot choose to traverse to odd or even indexed elements too.\n");
 
 
 
 
 
 
 
    } 
}
